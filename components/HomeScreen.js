import React, { Component } from "react";
import { Button } from "react-native";

export default class HomeScreen extends Component {
    static navigationOptions = {
      title: 'Mobil Alk Bead Home',
    };
    render() {
      const {navigate} = this.props.navigation;
      return (
        <view>
        <Button
          title="Screen1"
          color="orange"
          onPress={() => navigate('Screen1', {name: 'User'})}
        />

        <Button
          title="Screen2"
          color="darkblue"
          onPress={() => navigate('Screen2', {name: 'User'})}
        />

        <Button
          title="Screen3"
          onPress={() => navigate('Screen3', {name: 'User'})}
        />
        </view>
      );
    }
  }