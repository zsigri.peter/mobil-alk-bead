import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

import DataInput from "./DataInput";
import DataList from "./DataList";

export default class Screen1 extends Component {
    static navigationOptions = {
      title: 'Home',
    };
    state = {
    userInput: []
  };

  inputAddedHandler = userValue => {
    this.setState(prevState => {
      return {
        userInput: prevState.userInput.concat({
          key: Math.random(),
          value: userValue
        })
      };
    });
  };

  inputDeletedHandler = key => {
    this.setState(prevState => {
      return {
        userInput: prevState.userInput.filter(input => {
          return input.key !== key;
        })
      };
    });
  };

    render() {
      const {navigate} = this.props.navigation;
      return (
        <View style={styles.container}>
        <Text style={styles.title}>
          Lista
        </Text>
        <DataInput onInputAdded={this.inputAddedHandler} />
        <DataList
          userInput={this.state.userInput}
          onItemDeleted={this.inputDeletedHandler}
        />
      </View>
      );
    }
  }
  const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 26,
    backgroundColor: "#000",
    alignItems: "center",
    color: "#fff",
    justifyContent: "flex-start"
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 10,
  }
});