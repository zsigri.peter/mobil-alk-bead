import React, { Component } from "react";
import { StyleSheet,TouchableHighlight,Text,View, } from "react-native";

export default class Screen3 extends Component {
    static navigationOptions = {
      title: 'Home',
    };

constructor(props) {
    super(props)
    this.state = { count: 10 }
  }

  onPressPlus = () => {
    this.setState({
      count: this.state.count+1
    })
  }

  onPressMinus = () => {
    this.setState({
      count: this.state.count-1
    })
  }

    render() {
      const {navigate} = this.props.navigation;
      return (
        <View style={styles.container}>
        <TouchableHighlight
         style={styles.button}
         onPress={this.onPressPlus}
        >
         <Text> + </Text>
        </TouchableHighlight>
        <View style={[styles.countContainer]}>
          <Text style={[styles.countText]}>
            { this.state.count !== 0 ? this.state.count: null}
          </Text>
        </View>
        <TouchableHighlight
         style={styles.button}
         onPress={this.onPressMinus}
        >
         <Text> - </Text>
        </TouchableHighlight>
      </View>
      );
    }
  }

  const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  countContainer: {
    alignItems: 'center',
    padding: 10
  },
  countText: {
    color: '#FF00FF'
  }
})

