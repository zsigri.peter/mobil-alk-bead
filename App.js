import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Text } from 'react-native';

import HomeScreen from './components/HomeScreen';
import Screen1 from './components/Screen1';
import Screen2 from './components/Screen2';
import Screen3 from './components/Screen3';

class App extends Component {
  static navigationOptions = {
    title: 'Screen1',
  };
  render() {
    const {navigate} = this.props.navigation;
    return (
      <Text>Cucc</Text>
    );
  }
}

const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Screen1: {screen: Screen1},
  Screen2: {screen: Screen2},
  Screen3: {screen: Screen3},
});

export default App = createAppContainer(MainNavigator);
